document.addEventListener("DOMContentLoaded", function () {
    const inputFile = document.getElementById("fileItem");
    const imageDisplay = document.getElementById("imageDisplay");
  
    inputFile.addEventListener("change", function () {
      if (inputFile.files.length > 0) {
        const selectedFile = inputFile.files[0];
        const imageURL = URL.createObjectURL(selectedFile);
        imageDisplay.src = imageURL;
      }
    });
  });

