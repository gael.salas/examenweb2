const btnMostrarTabla = document.getElementById('btnMostrarTabla');
const btnLimpiar = document.getElementById('btnLimpiar');
let numeros = {
    "1": '<img src="/img/1.png">',
    "2":'<img src="/img/2.png">',
    "3":'<img src="/img/3.png">',
    "4":'<img src="/img/4.png">',
    "5":'<img src="/img/5.png">',
    "6":'<img src="/img/6.png">',
    "7":'<img src="/img/7.png">',
    "8":'<img src="/img/8.png">',
    "9":'<img src="/img/9.png">',
    "10":'<img src="/img/10.png">',
    "=":'<img src="/img/=.png">',
    "x":'<img src="/img/x.png">',
    "0":'<img src="/img/0.png">'
};

btnMostrarTabla.addEventListener("click", function(){  
     
    let datos = document.getElementById('mostrar');
    let tabla = document.getElementById('tabla').value;
    var result;
    
    for(let x=1;x<=10;x++){
        let fila = datos.insertRow();
        let c1 = fila.insertCell(0);
        let c2 = fila.insertCell(1);
        let c3 = fila.insertCell(2);
        let c4 = fila.insertCell(3);
        let c5 = fila.insertCell(4);
        c1.innerHTML = numeros[tabla];
        c2.innerHTML = numeros.x;
        c3.innerHTML = numeros[x];
        c4.innerHTML = numeros["="];
        result = tabla*x;
        c5.innerHTML = result;

    }
});

btnLimpiar.addEventListener("click",function(){
    let datos = document.getElementById('mostrar');
    datos.innerHTML=" ";
});

function valiUni(result){
    if((result/9)%2==1){
        return 9;
    }
    else if((result/8)%2==1){
        return 8;
    }
    else if((result/7)%2==1){
        return 7;
    }
    else if((result/6)%2==1){
        return 6;
    }
    else if((result/5)%2==1 && (result/10) < 1){
        return 5;
    }
    else if((result/4)%2==1 && (result/10) < 1){
        return 4;
    }
    else if((result/3)%2==1){
        return 3;
    }
    else if((result/2)%2==1 && (result/10) < 1 ){
        return 2;
    }
    else if((result/1)%2==1 ){
        return 1;
    }
    else{
        return 0;
    }
}
